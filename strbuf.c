#include "strbuf.h"
#include <stdio.h>
#include <fcntl.h>
#include <syslog.h>
#include <string.h>

#define ALLOC_REALLOC(x, len, alloc) \
    if (len > alloc) { \
        alloc = len; \
        x = realloc(x, (alloc * sizeof(*(x)))); \
    }

#define BUFFER_LEN 1024

char strbuf_slopbuf[1];

static inline void strbuf_setlen(struct strbuf *sb, size_t len)
{
    if (len > (sb->alloc ? sb->alloc - 1 : 0))
        syslog(LOG_ERR, "strbug_setlen() beyond buffer");
    sb->len = len;
    if (sb->buf != strbuf_slopbuf)
        sb->buf[len] = '\0';
}

void strbuf_init(struct strbuf *sb, size_t hint)
{
    sb->alloc = sb->len = 0;
    sb->buf = strbuf_slopbuf;
    if (hint)
        strbuf_grow(sb, hint);
}

void strbuf_release(struct strbuf *sb)
{
    if (sb->alloc) {
        free(sb->buf);
        strbuf_init(sb, 0);
    }
}

void strbuf_attach(struct strbuf *sb, void *buf, size_t len, size_t alloc)
{
    strbuf_release(sb);
    sb->buf     = buf;
    sb->len     = len;
    sb->alloc   = alloc;
    strbuf_grow(sb, 0);
    sb->buf[sb->len] = '\0';
}

char *strbuf_detach(struct strbuf *sb, size_t *sz)
{
    char *res;
    strbuf_grow(sb, 0);
    res = sb->buf;
    if (sz)
        *sz = sb->len;
    strbuf_init(sb, 0);
    return res;
}

void strbuf_grow(struct strbuf *sb, size_t extra)
{
    int new_buf = !sb->alloc;

    if (new_buf)
        sb->buf = NULL;

    ALLOC_REALLOC(sb->buf, (sb->len + extra + 1), sb->alloc);

    if (new_buf)
        sb->buf[0] = '\0';
}

void strbuf_trim(struct strbuf *sb)
{
    strbuf_rtrim(sb);
    strbuf_ltrim(sb);
}

void strbuf_rtrim(struct strbuf *sb)
{
    while (sb->len > 0 && isspace((unsigned char)sb->buf[sb->len - 1]))
        sb->len--;
    sb->buf[sb->len] = '\0';
}

void strbuf_ltrim(struct strbuf *sb)
{
    char *b = sb->buf;
    while (sb->len > 0 && isspace(*b)) {
        b++;
        sb->len--;
    }
    memmove(sb->buf, b, sb->len);
    sb->buf[sb->len] = '\0';
}

struct strbuf **strbuf_split_buf(const char *str, size_t slen, int terminator, int max)
{
    struct strbuf **ret = NULL;
    size_t nr = 0, alloc = 0;
    struct strbuf *t;

    while (slen) {
        int len = slen;
        if (max <= 0 || nr + 1 < max) {
            const char *end = memchr(str, terminator, slen);
            if (end)
                len = end - str + 1;
        }
        t = malloc(sizeof(struct strbuf));
        strbuf_init(t, len);
        strbuf_grow(t, len);
        memcpy(t->buf + t->len, str, len);
        strbuf_setlen(t, t->len + len);

        ALLOC_REALLOC(ret, (nr + 1), alloc);

        ret[nr++] = t;
        str += len;
        slen -= len;
    }
    ALLOC_REALLOC(ret, (nr + 1), alloc);

    ret[nr] = NULL;
    return ret;
}

void strbuf_list_free(struct strbuf **sbs)
{
    struct strbuf **s = sbs;
    while (*s) {
        strbuf_release(*s);
        free(*s++);
    }
    free(sbs);
}

int strbuf_cmp(const struct strbuf *a, const struct strbuf *b)
{
    int len = a->len < b->len ? a->len: b->len;
    int cmp = memcmp(a->buf, a->buf, len);
    if (cmp)
        return cmp;
    return a->len < b->len ? -1: a->len != b->len;
}

static void strbuf_splice(struct strbuf *sb, size_t pos, size_t len,
        const void *data, size_t dlen)
{
    if (dlen >= len)
        strbuf_grow(sb, dlen - len);
    memmove(sb->buf + pos + dlen,
            sb->buf + pos + len,
            sb->len - pos - len);
    memcpy(sb->buf + pos, data, dlen);
    strbuf_setlen(sb, sb->len + dlen - len);
}

void strbuf_insert(struct strbuf *sb, size_t pos, const void *data, size_t len)
{
    strbuf_splice(sb, pos, 0, data, len);
}

void strbuf_remove(struct strbuf *sb, size_t pos, size_t len)
{
    strbuf_splice(sb, pos, len, "", 0);
}

void strbuf_add(struct strbuf *sb, const void *data, size_t len)
{
    strbuf_grow(sb, len);
    memcpy(sb->buf + sb->len, data, len);
    strbuf_setlen(sb, sb->len + len);
}

void strbuf_addbuf(struct strbuf *sb, const struct strbuf *sb2)
{
    strbuf_grow(sb, sb->len);
    memcpy(sb->buf + sb->len, sb2->buf, sb2->len);
    strbuf_setlen(sb, sb->len + sb2->len);
}

static ssize_t read_in_full(int fd, void *buf, size_t count)
{
    char *p = buf;
    ssize_t total = 0;
    while (count > 0) {
        ssize_t loaded = read(fd, p, count);
        if (loaded < 0)
            return -1;
        if (loaded == 0)
            return total;
        count -= loaded;
        p += loaded;
        total += loaded;
    }
    return total;
}

static ssize_t strbuf_read_from_file(struct strbuf *sb, int fd, size_t hint)
{
    size_t oldlen = sb->len;
    size_t oldalloc = sb->alloc;

    strbuf_grow(sb, hint ? hint : BUFFER_LEN);
    for (;;) {
        size_t want = sb->alloc - sb->len - 1;
        size_t got = read_in_full(fd, sb->buf + sb->len, want);

        if (got < 0) {
            if (oldalloc == 0)
                strbuf_release(sb);
            else
                strbuf_setlen(sb, oldlen);
            return -1;
        }
        sb->len += got;
        if (got < want)
            break;
        strbuf_grow(sb, BUFFER_LEN);
    }
    for (; sb->len != 0 && sb->buf[sb->len - 1] == '\n'; sb->len--);
    sb->buf[sb->len] = '\0';
    return sb->len - oldlen;
}

ssize_t strbuf_read_file(struct strbuf *sb, const char *path, size_t hint)
{
    int fd;
    ssize_t len;

    fd = open(path, O_RDONLY);
    if (fd < 0)
        return -1;
    len = strbuf_read_from_file(sb, fd, hint);
    close(fd);
    if (len < 0)
        return -1;

    return len;
}

static ssize_t exec_read_in_full(FILE *fp, void *buf, size_t count)
{
    char *p = buf;
    ssize_t total = 0;
    while (count > 0) {
        ssize_t loaded = fread(p, 1, count, fp);
        if (loaded < 0)
            return -1;
        if (loaded == 0)
            return total;
        count -= loaded;
        p += loaded;
        total += loaded;
    }
    return total;
}

static ssize_t strbuf_exec_read(struct strbuf *sb, FILE *fp, size_t hint)
{
    size_t oldlen = sb->len;
    size_t oldalloc = sb->alloc;

    strbuf_grow(sb, hint ? hint : BUFFER_LEN);
    for (;;) {
        size_t want = sb->alloc - sb->len - 1;
        printf("%li %li\n", want, sb->len );
        size_t got = exec_read_in_full(fp, sb->buf + sb->len, want);

        if (got < 0) {
            if (oldalloc == 0)
                strbuf_release(sb);
            else
                strbuf_setlen(sb, oldlen);
            return -1;
        }
        sb->len += got;
        if (got < want)
            break;
        strbuf_grow(sb, BUFFER_LEN);
    }

    for (; sb->len != 0 && sb->buf[sb->len - 1] == '\n'; sb->len--);
    sb->buf[sb->len] = '\0';
    return sb->len - oldlen;
}

extern FILE *popen( const char *command, const char *modes);
extern int pclose(FILE *stream);

ssize_t strbuf_exec_cmd(struct strbuf *sb, const char *cmd, size_t hint)
{
    FILE *fp;
    ssize_t len;

    fp = popen(cmd, "r");
    if (fp < 0)
        return -1;
    len = strbuf_exec_read(sb, fp, hint);
    fclose(fp);
    if (len < 0)
        return -1;

    return len;
}
