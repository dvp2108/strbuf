#ifndef STRBUF_H
#define STRBUF_H

#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <ctype.h>

struct strbuf {
    size_t alloc;
    size_t len;
    char *buf;
};

extern char strbuf_slopbuf[];
#define STRBUF_INIT { .alloc = 0, .len = 0, .buf = strbuf_slopbuf }

extern void strbuf_init(struct strbuf *, size_t);

extern void strbuf_release(struct strbuf *);

extern void strbuf_attach(struct strbuf *, void *, size_t, size_t);

extern char *strbuf_detach(struct strbuf *, size_t *);

extern ssize_t strbuf_read_file(struct strbuf *, const char *, size_t);

extern ssize_t strbuf_exec_cmd(struct strbuf *, const char *, size_t);

extern void strbuf_grow(struct strbuf *, size_t);

static inline size_t strbuf_avail(const struct strbuf *sb)
{
    return sb->alloc ? sb->alloc - sb->len - 1 : 0;
}

extern void strbuf_trim(struct strbuf *);

extern void strbuf_rtrim(struct strbuf *);

extern void strbuf_ltrim(struct strbuf *);

extern int strbuf_cmp(const struct strbuf *, const struct strbuf *);

extern struct strbuf **strbuf_split_buf(const char *, size_t, int, int);

extern void strbuf_list_free(struct strbuf **);

extern void strbuf_insert(struct strbuf *, size_t, const void *, size_t);

extern void strbuf_remove(struct strbuf *, size_t, size_t);

extern void strbuf_add(struct strbuf *, const void*, size_t);

extern void strbuf_addbuf(struct strbuf *, const struct strbuf*);

#endif
